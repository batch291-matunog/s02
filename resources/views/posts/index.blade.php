@extends('layouts.app')

@section('content')
    @if(count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card text-center mb-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>

                @if(Auth::user())
					@if(Auth::user()->id == $post->user->id)
						<div class="card-footer">
							<div>
							<form method="POST" action="/posts/{{$post->id}}">
								@method('PUT')
								@csrf
								<a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>

								{{-- S04 Activity solution Version 2--}}
								 @if($post->isActive == true)

                                    <a href="/posts/{{$post->id}}/archive" class="btn btn-danger">Archive Post</a>
                                @else
                                    <a href="/posts/{{$post->id}}/unarchive" class="btn btn-success">Unarchive Post</a>
                                @endif
                                {{-- <button type="submit" class="btn btn-danger">Delete Post</button> --}}
							</form>

						</div>
					@endif
				@endif

            </div>
        @endforeach

    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif

@endsection
