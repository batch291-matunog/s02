<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access the authenticated user via Auth Facade
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()) {
            // instantiate a new post object from the Post Model
            $post = new Post;
            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            // $post->image = $request->input('image');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key for the user_id of the new post.
            $post->user_id = (Auth::user()->id);
            // save this $post object into the database.
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts.
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    // action that will return a view showing three random blog posts in the welcome page.
    public function getFeaturedPosts()
    {
        $featuredPosts = Post::where('isActive', true)->inRandomOrder()->take(3)->get();
        return view('welcome')->with('featuredPosts', $featuredPosts);
    }

    // action for showing only the posts authored by the authenticated user.
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    // action that will get the specific post when clicked the edit button
    public function edit($id)
    {
        $post = Post::find($id);

        // Check if the authenticated user is the owner of the post
        if (Auth::user()) {
            if ($post->user_id == Auth::user()->id) {
                return view('posts.edit')->with('post', $post);
            }

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will update the specific post
    public function update(Request $request, $id)
    {
        if(Auth::user()){
            // Retrieve the post by ID and update its attributes
            $post = Post::find($id);

            if(Auth::user()->id == $post->user_id) {
                $post->title = $request->input('title');
                $post->content = $request->input('content');

                // Save the updated post to the database
                $post->save();
            }

            // Redirect the user to the post's page with a success message
            return redirect('/posts');
        }
    }

    // public function destroy($id)
    // {
    //     $post = Post::find($id);

    //     if(Auth::user()->id == $post->user_id){
    //         $post->delete();
    //     }

    //     return redirect('/posts');
    // }

    // action that will archive a post
    public function archive($id)
    {
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = false; // set isActive property to false
            $post->save(); // save the updated post
        }

        return redirect('/myPosts');
    }

    public function unarchive($id)
    {
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = true; // set isActive property to true
            $post->save(); // save the updated post
        }

        return redirect('/posts');
    }

    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // if authenticated user is not the post author
        if($post->user_id != $user_id){
            // checks if a post like has been made by the  logged in user before
            if($post->likes->contains("user_id", $user_id)) {
                // deletes the like made by the user to unlike the post.
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                // to instantiate a new PostLike object from the PostLike model.
                $postLike = new PostLike;
                // define the properties of the $postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                // save the $postLike object into the database.
                $postLike->save();
            }
            // redirect user back to the original post.
            return redirect("/posts/$id");
        }
    }

    public function comment(Request $request, $id)
    {
        $post = Post::find($id);

        if(Auth::user()) {
            // Find the post with the given ID
            $postComment = new PostComment;
            $postComment->content = $request->input('content');
            $postComment->user_id = Auth::user()->id;
            $postComment->post_id = $post->id;

            // save this $post object into the database.
            $postComment->save();

            return redirect("/posts/$id");
        } else {
            return redirect('/login');
        }
    }
}
