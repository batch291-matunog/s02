@extends('layouts.app')

@section('content')
    <div class="card mb-5">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created at {{$post->created_at}}</p>
            {{-- <img src="{{$post->image}}" alt="image"> --}}
            <p class="card-text">{{$post->content}}</p>

            <p class="card-text">Likes: {{ $post->likes()->count() }} | Comments: {{ $post->comments()->count() }}</p>

            @if (Auth::id() != $post->user_id)
                <form class="d-inline" action="/posts/{{$post->id}}/like" method="POST">
                    @method('PUT')
                    @csrf

                    @if ($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">
                            Unlike
                        </button>
                    @else
                        <button type="submit" class="btn btn-success">
                            Like
                        </button>
                    @endif

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Comment
                    </button>
                </form>
            @endif

            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form action="/posts/{{$post->id}}/comment" method="POST">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Leave a Comment</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="content">Comment:</label>
                    <textarea class="form-control" name="content" id="content" rows="3"></textarea>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Comment</button>
        </div>
        </div>
        </form>
    </div>
    </div>
@endsection

@section('comments')
    @foreach($post->comments as $comment)
        <div class="card text-center mb-3 py-3">
            <div class="card-body">
                <h3 class="card-title mb-3 text-center">
                    {{$comment->content}}
                </h3>
                <h4 class="card-text mb-3 text-end">Posted by: {{$post->user->name}}</h4>
                <p class="card-subtitle mb-3 text-muted text-end">Posted on: {{$post->created_at}}</p>
            </div>
        </div>
    @endforeach
@endsection
