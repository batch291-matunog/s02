@extends('layouts.app')


@section('content')

    <form action="/posts" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="form-control">
        </div>
        {{-- <div class="form-group">
            <label for="image">Image:</label>
            <input type="text" name="image" id="image" class="form-control">
        </div> --}}
        <div class="form-group">
            <label for="content">Content:</label>
            <textarea class="form-control" name="content" id="content" rows="3"></textarea>
        </div>
        <div class="mt-2">
            <button type="submit" class="btn btn-primary">Create Post</button>
        </div>
    </form>

@endsection
