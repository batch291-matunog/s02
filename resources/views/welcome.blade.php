@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-center align-items-center">
        <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" class="w-50 img-fluid">
    </div>

    <div class="d-flex justify-content-center align-items-center mt-5">
        <div class="w-75">
            <h2 class="text-center mb-3">Featured Posts</h2>

            <div class="row">
                @if (count($featuredPosts) > 0)
                    @foreach ($featuredPosts as $post)
                        <div class="card text-center mb-2">
                            <div class="card-body">
                                <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                                <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                            </div>
                        </div>
                    @endforeach

                 @else
                    <div class="text-center">
                        <h2>There are no posts to show</h2>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

